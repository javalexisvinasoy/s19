function printLoginMessage(){

let username = prompt("Please enter your username").toLowerCase();
let password = prompt("Please enter your password").toLowerCase();
let role = prompt("Please enter your role").toLowerCase();

if (!username || !password || !role) {
    alert("Inputs should not be empty!");
  } else {

    switch (role) {
      case "admin":
        alert("Welcome back to the class portal, " + role + "!");
        break;
      case "teacher":
        alert("Thank you for logging in, " + role + "!");
        break;

      case "student":
        alert("Welcome to the class portal, " + role + "!");
        break;

      default:
        alert("Role out of range.");
        break;
    }
  }
}

printLoginMessage()

// Actvity part 2

function getAverage(...numbers){
    let x = 1;
    let totalScore = 0;

    for(const number of numbers){
        totalScore += number;
        x++;
    }

     let average = Math.round(totalScore / (x - 1));
    console.log(average);

    if(average <= 74){
        console.log("Hello, student, your average is " +  average + ". The letter equivalent is F");
    }else if(average >= 75 && average <= 79){
        console.log("Hello, student, your average is " +  average + ". The letter equivalent is D");

    }else if(average >= 80 && average <= 84){
        console.log("Hello, student, your average is " +  average + ". The letter equivalent is C");

    }else if(average >= 85 && average <= 89){
        console.log("Hello, student, your average is " +  average + ". The letter equivalent is B");

    }else if(average >= 90 && average >= 96){
        console.log("Hello, student, your average is " +  average + ". The letter equivalent is A");

    }else {
        console.log("Hello, student, your average is " +  average + ". The letter equivalent is A+");
    }
}

getAverage(75,80,94,93);