// alert("Hello");

/*
	Selection Control Structures
	- it sorts out whether the statement/s are to be executed based on the condition whether it is true or false

	> two-way selection (true or false)
	> multi-way selection

*/

// if else statements

/*
	syntax:

		if(condition){
			// statement
		} else if (condition) {
			// statement
		} else {
			// statement
		}
*/

/*
	if statement - executes a statement if a specified condition is true
*/

let numA = -1;

if(numA < 0){
	console.log("Hello");
}

console.log(numA < 0);

let city = "New York";

if (city === "New York"){
	console.log("Welcome to New York City!")
}

/*
	else if 
		- it executes a statement if our previous conditions are false and if the specified condition is true
		- the "else if" clause is option and can be added to capture additional  conditions to change the flow of a program
*/

let numB = 1;

if (numA > 0){
	console.log("Hello!");
} else if (numB > 0){
	console.log("World");
}

city = "Tokyo";
 
 if (city === "New York"){
 	console.log("Welcome to New York City!");
 } else if (city === "Tokyo") {
 	console.log("Welcome to Tokyo!")
 };


 /*
	else statement
	 	- executes a statement if all of our previous conditons are false
 */

if (numA > 0){
	console.log("Hello!");
} else if (numB === 0){
	console.log("World");
} else {
	console.log("Again");
};

/*let age = parseInt(prompt("Enter your age:"));

if(age <= 18) {
	console.log("Not allowed to drink");
} else {
	console.log("Congratulations! You can now take a shot!");
};*/

// MINI ACTIVITY

/*let height = parseInt(prompt("Enter your height:"));

if (height <= 150) {
	console.log("Did not pass the min height")
} else {
	console.log("Passed the min height requirement")
};*/

function minHeight (height) {
	if (height <= 150) {
		console.log("Did not pass the min height")
	} else {
		console.log("Passed the min height requirement")
	}
}

minHeight(145);

// Nested if statement

let isLegalAge = true;

let isAdmin = false;

if(isLegalAge) {
	if (!isAdmin) {
		console.log("You are not an admin!")
	}
};

let message = "No message";


function determineTyphoonIntensity(windSpeed) {
	if (windSpeed < 30){
		return "Not a typhoon"
	} else if (windSpeed <= 61) {
		return "Tropical depression detected.";
	} else if (windSpeed >= 62 && windSpeed <= 88 ) {
		return "Tropical Storm detected.";
	} else if (windSpeed >= 89 && windSpeed <= 177) {
		return "Severe Tropical Storm detected.";
	} else {
		return "Typhoon Detected!"
	}
};

message = determineTyphoonIntensity(70);
console.log(message);

message = determineTyphoonIntensity(178);
console.log(message);



// Truthy and Falsy

/*
	In JS, a truthy value is a value that is considered true when encountered in a boolean context.

	Falsy Values:
		1. false
		2. 0
		3. -0
		4. "" / or empty
		5. null
		6. undefined
		7. NaN
*/


// Truthy Examples:

let word = "true";

if(word){
	console.log("Truthy");
}

if(true){
	console.log("Truthy");
}

if(1){
	console.log("Truthy");
}

if([]){
	console.log("Truthy");
}



// Falsy Examples:

if(false){
	console.log("Falsy");
}

if(0){
	console.log("Falsy");
}

if(-0){
	console.log("Falsy");
}

if(undefined){
	console.log("Falsy");
}

if(null){
	console.log("Falsy");
}

if(NaN){
	console.log("Falsy");
}

// Conditional (Ternary) operator - for short codes

/*
	Ternary operator takes in three operands
		1. conditon
		2. expression to execute if condition is true/truthy
		3. expression to execute if the condition is false/falsy.

	Syntax:
		(condition) ? ifTrue_expression : ifFalse_expression	
*/

// Single Statement Execution
let ternaryResult = (1<18) ? "Condition is True" : "Conditon is False"

console.log("Result of the Ternary Operator: " + ternaryResult);

let name;

function isOfLegalAge(){
	name = "John";
	return "You are of the legal age limit";
}

function isUnderAge (){
	name = "Jane";
	return "You are under the age limit"
}

let age = parseInt(prompt("What is your age?"));
console.log(age);

let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();

console.log("Result of Ternary Operator in Function: " + legalAge + ", " + name);



// Switch Statement
/*
	Can be used as an aleternative to an if ... else statement where the date to eb used in the condition of an expected input:

	Syntax:

		switch (expression) {
			case <value>:
				statement;
				break;
			default:
				statement;
				break;	
		}
*/

let day = prompt("What day of the week is it today?").toLowerCase()

console.log(day);

switch(day) {
	case "monday":
	console.log("The color of the day is red.");
		break;

	case "tuesday":
	console.log("The color of the day is orange.");
		break;

	case "wednesday":
	console.log("The color of the day is yellow.");
		break;	

	case "thursday":
	console.log("The color of the day is green.");
		break;

	case "friday":
	console.log("The color of the day is blue.");
		break;

	case "saturday":
	console.log("The color of the day is indigo.");
		break;

	case "sunday":
	console.log("The color of the day is violet.");
		break;

	default:
		console.log("Please input a valid day");
		break;
}

// break - is for ending the loop
// switch - is not a very popular statement in coding.


// Try-Catch-Finally Statement
/*
	- try-catch is commonly used for error handling.
	- will still function even if the statement is not complete
*/

function showIntensityAlert(windSpeed){
	try {
		alert(determineTyphoonIntensity(windSpeed))
	}
	catch (error){
		console.log(typeof error);
		console.log(error);
		console.warn(error.message);
	}
	finally {
		alert("Intensity updates will show new alert!")
	}
}

showIntensityAlert(56);